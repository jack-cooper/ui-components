# Example UI Components

A site I built in a week between Aug 24th and Aug 31st 2020.

## Live at

https://ui-components-6e371.web.app/

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```
