import { computed } from 'vue'
import routeNames from '@/constants/routeNames'
import router from '@/router'

const currentRouteIndex = computed(() =>
  routeNames.findIndex((name) => name === router.currentRoute.value.name)
)

const goNextPage = () => {
  const newRouteName = routeNames[currentRouteIndex.value + 1]
  router.push({
    name: newRouteName,
    params:
      newRouteName === 'NotFound'
        ? { catchAll: 'not-found-example' }
        : undefined,
  })
}
const goPreviousPage = () =>
  router.push({ name: routeNames[currentRouteIndex.value - 1] })

let xStart: number

const handleTouchStart = (event: TouchEvent) => {
  xStart = event.touches[0].clientX
}
const handleTouchEnd = (event: TouchEvent) => {
  const xEnd = event.changedTouches[0].clientX
  const xDiff = xStart - xEnd

  if (Math.abs(xDiff) > 100) {
    xDiff > 0 ? goNextPage() : goPreviousPage()
  }
}

const setupNavigationListeners = () => {
  window.addEventListener('keyup', ({ key }) => {
    if (
      key === 'ArrowRight' &&
      currentRouteIndex.value < routeNames.length - 1
    ) {
      goNextPage()
    }
    if (key === 'ArrowLeft' && currentRouteIndex.value > 0) {
      goPreviousPage()
    }
  })

  window.addEventListener('touchstart', handleTouchStart, false)
  window.addEventListener('touchend', handleTouchEnd, false)
}

export default setupNavigationListeners
