interface SkillsCard {
  alt: string
  src: string
  title: string
}

export default [
  {
    title: 'Vue',
    src: 'vue.svg',
    alt: 'Vue Logo',
  },
  {
    title: 'React',
    src: 'react.svg',
    alt: 'React Logo',
  },
  {
    title: 'JavaScript',
    src: 'js.svg',
    alt: 'JavaScript Logo',
  },
  {
    title: 'Node',
    src: 'node.svg',
    alt: 'Node Logo',
  },
  {
    title: 'Docker',
    src: 'docker.png',
    alt: 'Docker Logo',
  },
  {
    title: 'TypeScript',
    src: 'ts.svg',
    alt: 'TypeScript Logo',
  },
  {
    title: 'Java',
    src: 'java.svg',
    alt: 'Java Logo',
  },
] as Array<SkillsCard>
