interface ContactInfoCard {
  alt: string
  description: string
  href: string
  src: string
  title: string
}

export default [
  {
    alt: '@ symbol',
    description: 'cooper.jackwilliam@gmail.com',
    href: 'mailto:cooper.jackwilliam@gmail.com',
    src: 'at.svg',
    title: 'Email',
  },
  {
    alt: 'GitLab logo',
    description: 'gitlab.com/jack-cooper',
    href: 'https://gitlab.com/jack-cooper',
    src: 'gitlab-icon-rgb.svg',
    title: 'GitLab',
  },
  {
    alt: 'LinkedIn Logo',
    description: 'linkedin.com/in/jackwcooper',
    href: 'https://www.linkedin.com/in/jackwcooper',
    src: 'linkedin.png',
    title: 'LinkedIn (Inactive)',
  },
  {
    alt: 'Code slash symbol',
    description: 'Of Varying Quality',
    href: 'https://jwcooper.net',
    src: 'code.svg',
    title: 'Old Projects',
  },
] as Array<ContactInfoCard>
