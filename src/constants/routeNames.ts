import { routes } from '@/router'

const routeNames = routes.map(({ name }) => name as string)

export default routeNames
